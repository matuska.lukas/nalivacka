/**
 * Category controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */

/**
 * Models
 */
const Category = require('../models/Category')

module.exports.new = (req, res) => {
  if (req.body.name === undefined) {
    return res.send('not-send-name')
  }
  new Category({
    name: req.body.name,
    author: req.session.user._id
  }).save((err) => {
    if (err) {
      console.error(err)
      return res.send('err')
    }
    res.send('ok')
  })
}

module.exports.updateById = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  } else if (req.body.update === undefined) {
    return res.send('not-send-updated-object')
  }
  Category
    .findByIdAndUpdate(
      req.body.id,
      req.body.update,
      (err) => {
        if (err) {
          console.error(err)
          return res.send(err)
        }
        res.send('ok')
      })
}

module.exports.deleteById = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  }
  Category
    .findByIdAndDelete(
      req.body.id,
      (err) => {
        if (err) {
          console.error(err)
          return res.send(err)
        }
        res.send('ok')
      })
}

module.exports.list = (req, res) => {
  if (req.query.filter) {
    req.query.filter = {}
  }
  Category
    .find(req.query.filter)
    .populate({
      path: 'author',
      select: 'name email photo type'
    })
    .exec((err, categories) => {
      if (err) {
        res.send(err)
        return console.error(err)
      }
      return res.status(200).json(categories)
    })
}
