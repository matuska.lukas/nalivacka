/**
 * Result controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */

/**
 * Models
 */
const Result = require('../models/Result')

module.exports.new = (req, res) => {
  if (req.body.type === undefined) {
    return res.send('not-send-type')
  } else if (req.body.team === undefined) {
    return res.send('not-send-team')
  } else if (req.body.category === undefined) {
    return res.send('not-send-category')
  }
  new Result({
    attemps: [],
    type: req.body.type,
    team: req.body.team,
    category: req.body.category,
    style: req.body.style,
    year: req.session.year._id,
    author: req.session.user._id
  }).save((err) => {
    if (err) {
      console.error(err)
      return res.send('err')
    }
    res.send('ok')
  })
}

module.exports.updateById = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  }
  const update = {}
  if (req.body.type !== undefined) {
    update.type = req.body.type
  }
  if (req.body.team !== undefined) {
    update.team = req.body.team
  }
  if (req.body.category !== undefined) {
    update.category = req.body.category
  }
  if (req.body.year !== undefined) {
    update.year = req.body.year
  }
  if (req.body.attempts !== undefined) {
    update.attempts = req.body.attempts
  }
  if (req.body.style !== undefined) {
    update.style = req.body.style
  }
  Result
    .findByIdAndUpdate(
      req.body.id,
      update,
      (err) => {
        if (err) {
          console.error(err)
          return res.send('err')
        }
        res.send('ok')
      }
    )
}

module.exports.addAttemp = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  } else if (req.body.attemp === undefined) {
    return res.send('not-send-attemp')
  }
  Result
    .findByIdAndUpdate(
      req.body.id,
      {
        $push: {
          attemps: req.body.attemp
        }
      },
      (err) => {
        if (err) {
          console.error(err)
          return res.send('err')
        }
        res.send('ok')
      }
    )
}

module.exports.deleteById = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  }
  Result
    .findByIdAndDelete(
      req.body.id,
      (err) => {
        if (err) {
          console.error(err)
          return res.send(err)
        }
        res.send('ok')
      })
}

module.exports.list = (req, res) => {
  if (req.query.filter === undefined) {
    req.query.filter = {}
  }
  Result
    .find(req.query.filter)
    .populate('team')
    .populate('category')
    .populate('year')
    .populate({
      path: 'author',
      select: 'name email photo type'
    })
    .exec((err, categories) => {
      if (err) {
        res.send(err)
        return console.error(err)
      }
      return res.status(200).json(categories)
    })
}
