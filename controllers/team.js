/**
 * Team controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */

/**
 * Models
 */
const Team = require('../models/Team')

module.exports.new = (req, res) => {
  if (req.body.name === undefined) {
    return res.send('not-send-name')
  } else if (req.body.basket === undefined) {
    return res.send('not-send-basket')
  } else if (req.body.pourer === undefined) {
    return res.send('not-send-pourer')
  } else if (req.body.category === undefined) {
    return res.send('not-send-category')
  }
  new Team({
    name: req.body.name,
    basket: req.body.basket,
    pourer: req.body.pourer,
    category: req.body.category,
    priority: req.body.priority,
    author: req.session.user._id,
    year: req.session.year._id
  }).save((err) => {
    if (err) {
      console.error(err)
      return res.send('err')
    }
    res.send('ok')
  })
}

module.exports.updateById = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  }
  const update = {}
  if (req.body.name !== undefined) {
    update.name = req.body.name
  }
  if (req.body.basket !== undefined) {
    update.basket = req.body.basket
  }
  if (req.body.pourer !== undefined) {
    update.pourer = req.body.pourer
  }
  if (req.body.category !== undefined) {
    update.category = req.body.category
  }
  Team
    .findByIdAndUpdate(
      req.body.id,
      update,
      (err) => {
        if (err) {
          console.error(err)
          return res.send('err')
        }
        res.send('ok')
      }
    )
}

module.exports.deleteById = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  }
  Team
    .findByIdAndDelete(
      req.body.id,
      (err) => {
        if (err) {
          console.error(err)
          return res.send(err)
        }
        res.send('ok')
      })
}

module.exports.list = (req, res) => {
  if (req.query.filter) {
    req.query.filter = {}
  }
  Team
    .find(req.query.filter)
    .populate('category')
    .populate({
      path: 'author',
      select: 'name email photo type'
    })
    .exec((err, categories) => {
      if (err) {
        res.send(err)
        return console.error(err)
      }
      return res.status(200).json(categories)
    })
}
