/**
 * Page controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */

/**
 * Models
 */
const Category = require('../models/Category')
const Result = require('../models/Result')
const User = require('../models/User')
const Team = require('../models/Team')
const Year = require('../models/Year')

module.exports.homepage = (req, res) => {
  res.render('homepage', { req, res, active: 'home', title: '' })
}

module.exports.login = (req, res) => {
  res.render('login', { req, res, active: 'login', title: 'Přihlášení' })
}

module.exports.register = (req, res) => {
  res.render('register', { req, res, active: 'login', title: 'Registrace' })
}

module.exports.results = (req, res) => {
  res.render('results', { req, res, active: 'results', title: 'Výsledky' })
}

module.exports.profile = (req, res) => {
  res.render('profile', { req, res, active: 'profile', title: 'Profil' })
}

module.exports.error = {
  accessDenied: (req, res) => {
    res
      .status(403)
      .render('error', { req, res, active: 'error-403', title: '403 Přístup odepřen!', description: 'Snažíte se dostat, kam Vám nebyly přiděleny práva, kontaktujte prosím správce.' })
  },
  notFound: (req, res) => {
    res
      .status(404)
      .render('error', { req, res, active: 'error-404', title: '404 Nenalezeno', description: 'Hledáte soubor, který se tu nenachází, přeji Vám příjmenou hru na schovávanou.' })
  },
  internalError: (req, res, error = 'Testovací stránka') => {
    res
      .status(500)
      .render('error', { req, res, active: 'error-500', title: '500 Vnitřní chyba serveru', description: error })
  }
}

module.exports.admin = {
  dashboard: (req, res) => {
    res.render('admin/dashboard', { req, res, active: 'dashboard', title: 'Přehled' })
  },

  profile: (req, res) => {
    res.render('admin/profile', { req, res, active: 'profile', title: 'Profil' })
  },

  error: {
    accessDenied: (req, res) => {
      res.render('admin/errors/403', { req, res, active: 'error', title: '403 Přístup odepřen' })
    },
    notFound: (req, res, title = '404 Nenalezeno', description = 'Hledáte soubor, který se tu nenachází, přeji Vám příjmenou hru na schovávanou.') => {
      res.render('admin/errors/404', { req, res, active: 'error', title, description })
    },
    internalError: (req, res) => {
      res.render('admin/errors/500', { req, res, active: 'error', title: '500 Chyba serveru' })
    }
  },

  categories: {
    list: (req, res) => {
      res.render('admin/categories/list', { req, res, active: 'categories', title: 'Seznam kategorií' })
    },
    new: (req, res) => {
      res.render('admin/categories/new', { req, res, active: 'categories', title: 'Nová kategorie' })
    },
    edit: (req, res) => {
      Category
        .findById(req.params.id)
        .populate('year')
        .populate('author')
        .exec((err, category) => {
          if (err) {
            this.admin.error.internalError(req, res)
            return console.error(err)
          }
          if (category === null) {
            return this.admin.error.notFound(
              req, res,
              '404 Kategorie nenalezena',
              'Hledáte kategorii, která se nenachází v databázi, přeji Vám příjmenou hru na schovávanou.'
            )
          }
          res.render('admin/categories/edit', { req, res, active: 'categories', title: 'Editace kategorie', category })
        })
    }
  },

  teams: {
    list: (req, res) => {
      res.render('admin/teams/list', { req, res, active: 'teams', title: 'Seznam týmů' })
    },
    new: (req, res) => {
      res.render('admin/teams/new', { req, res, active: 'teams', title: 'Nový tým' })
    },
    edit: (req, res) => {
      Team
        .findById(req.params.id)
        .populate('category')
        .populate('author')
        .exec((err, team) => {
          if (err) {
            this.admin.error.internalError(req, res)
            return console.error(err)
          }
          if (team === null) {
            return this.admin.error.notFound(
              req, res,
              '404 Tým nenalezen',
              'Hledáte tým, který se nenachází v databázi, přeji Vám příjmenou hru na schovávanou.'
            )
          }
          res.render('admin/teams/edit', { req, res, active: 'teams', title: 'Editace týmu', team })
        })
    }
  },

  results: {
    list: (req, res) => {
      res.render('admin/results/list', { req, res, active: 'results', title: 'Seznam výsledků' })
    },
    new: (req, res) => {
      res.render('admin/results/new', { req, res, active: 'results', title: 'Nový výsledek' })
    },
    edit: (req, res) => {
      Result
        .findById(req.params.id)
        .populate('team')
        .populate('category')
        .populate('year')
        .populate('author')
        .exec((err, result) => {
          if (err) {
            this.admin.error.internalError(req, res)
            return console.error(err)
          }
          if (result === null) {
            return this.admin.error.notFound(
              req, res,
              '404 Výsledek nenalezen',
              'Hledáte výsledek, který se nenachází v databázi, přeji Vám příjmenou hru na schovávanou.'
            )
          }
          res.render('admin/results/edit', { req, res, active: 'results', title: 'Editace výsledku', result })
        })
    }
  },

  users: {
    list: (req, res) => {
      res.render('admin/users/list', { req, res, active: 'users', title: 'Seznam uživatelů' })
    },
    new: (req, res) => {
      res.render('admin/users/new', { req, res, active: 'users', title: 'Nový uživatel' })
    },
    edit: (req, res) => {
      User
        .findById(req.params.id)
        .populate('years.year')
        .exec((err, user) => {
          if (err) {
            this.admin.error.internalError(req, res)
            return console.error(err)
          }
          if (user === null) {
            return this.admin.error.notFound(
              req, res,
              '404 Uživatel nenalezen',
              'Hledáte uživatele, který se nenachází v databázi, přeji Vám příjmenou hru na schovávanou.'
            )
          }
          Year
            .find({})
            .exec((err, years) => {
              if (err) {
                this.admin.error.internalError(req, res)
                return console.error(err)
              }
              res.render('admin/users/edit', { req, res, active: 'users', title: 'Editace uživatele', user, years })
            })
        })
    }
  },

  years: {
    list: (req, res) => {
      res.render('admin/years/list', { req, res, active: 'years', title: 'Seznam ročníků' })
    },
    new: (req, res) => {
      res.render('admin/years/new', { req, res, active: 'years', title: 'Nový ročník' })
    },
    edit: (req, res) => {
      Year
        .findById(req.params.id)
        // .populate('author')
        .exec((err, year) => {
          if (err) {
            this.admin.error.internalError(req, res)
            return console.error(err)
          }
          if (year === null) {
            return this.admin.error.notFound(
              req, res,
              '404 Ročník nenalezen',
              'Hledáte ročník, který se tu nenachází, přeji Vám příjmenou hru na schovávanou.'
            )
          }
          res.render('admin/years/edit', { req, res, active: 'years', title: 'Editace ročníku', year })
        })
    }
  }
}
