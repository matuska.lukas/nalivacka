var API = {
  user: {
    new: function (
      email,
      password,
      firstname,
      lastname,
      middlename,
      usertype) {
      return $.post(
        '/api/user/new',
        {
          email: email,
          password: password,
          firstname: firstname,
          middlename: middlename,
          lastname: lastname,
          usertype: usertype
        },
        'json'
      )
    },

    login: function (email, password) {
      return $.post(
        '/api/user/login',
        {
          email: email,
          password: password
        },
        'json'
      )
    },

    edit: function (object) {
      return $.post(
        '/api/user/edit',
        object,
        'json'
      )
    },

    forgotPassword: function (email) {
      return $.post(
        '/api/user/forgot-password',
        {
          email: email
        },
        'json'
      )
    },

    setNewPassword: function (userId, password) {
      return $.post(
        '/api/user/set-new-password',
        {
          id: userId,
          password: password
        },
        'json'
      )
    },

    updateSession: function () {
      return $.get('/api/user/update-session', {}, 'json')
    },

    changeType: function (userId, type) {
      return $.post(
        '/api/user/change-type',
        {
          id: userId,
          type: type
        },
        'json'
      )
    },

    list: function () {
      return $.get('/api/user/list', {}, 'json')
    },

    logout: function () {
      return $.get('/api/user/logout', {}, 'json')
    },

    delete: function (userId) {
      return $.post(
        '/api/user/delete',
        {
          id: userId
        },
        'json'
      )
    },

    loggedIn: function () {
      return $.get('/api/user/am-i-logged-in', {}, 'json')
    }
  },

  year: {
    new: function (name, description, status) {
      return $.post(
        '/api/year/new',
        {
          name: name,
          description: description,
          status: status
        },
        'json'
      )
    },

    edit: function (yearId, name, description, status) {
      return $.post(
        '/api/year/edit',
        {
          id: yearId,
          name: name,
          description: description,
          status: status
        },
        'json'
      )
    },

    list: function () {
      return $.get('/api/year/list', {}, 'json')
    },

    delete: function (yearId) {
      return $.post(
        '/api/year/delete',
        {
          id: yearId
        },
        'json'
      )
    },

    switch: function (yearId) {
      return $.post(
        '/api/year/switch',
        {
          id: yearId
        },
        'json'
      )
    }
  },

  category: {
    new: function (name) {
      return $.post(
        '/api/category/new',
        {
          name: name
        },
        'json'
      )
    },

    edit: function (id, name) {
      return $.post(
        '/api/category/update-by-id',
        {
          id: id,
          update: {
            name: name
          }
        },
        'json'
      )
    },

    delete: function (id) {
      return $.post(
        '/api/category/delete-by-id',
        {
          id: id
        },
        'json'
      )
    },

    list: function (filter) {
      if (filter === undefined) {
        filter = {}
      }
      return $.get('/api/category/list', filter, 'json')
    }
  },

  team: {
    new: function (name, basket, pourer, category, priority) {
      return $.post(
        '/api/team/new',
        {
          name: name,
          basket: basket,
          pourer: pourer,
          category: category,
          priority: priority
        },
        'json'
      )
    },

    edit: function (id, update) {
      update.id = id
      return $.post(
        '/api/team/update-by-id',
        update,
        'json'
      )
    },

    delete: function (id) {
      return $.post(
        '/api/team/delete-by-id',
        {
          id: id
        },
        'json'
      )
    },

    list: function (filter) {
      if (filter === undefined) {
        filter = {}
      }
      return $.get('/api/team/list', { filter: filter }, 'json')
    }
  },

  result: {
    new: function (type, team, category, style) {
      return $.post(
        '/api/result/new',
        {
          type: type,
          team: team,
          category: category,
          style: style
        },
        'json'
      )
    },

    edit: function (id, update) {
      update.id = id
      return $.post(
        '/api/result/update-by-id',
        update,
        'json'
      )
    },

    addAttempt: function (id, time) {
      return $.post(
        '/api/result/add-attemp',
        {
          id: id,
          attemp: time
        },
        'json'
      )
    },

    delete: function (id) {
      return $.post(
        '/api/result/delete-by-id',
        {
          id: id
        },
        'json'
      )
    },

    list: function (filter) {
      if (filter === undefined) {
        filter = {}
      }
      return $.get('/api/result/list', { filter: filter }, 'json')
    }
  }
}
