/**
 * Admin router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router()

/**
 * Libraries
 */
const moment = require('moment')
moment.locale('cs')

/**
 * Controllers
 */
const adminPageController = require('../controllers/page').admin
const pageController = require('../controllers/page')

/**
 * Routes
 */

/**
 * Make admin section a little bit secret
 */
router.get('/*', (req, res, next) => {
  if (req.session.user !== undefined) {
    if (req.session.user.type === 'admin') {
      return next()
    }
  }
  pageController.error.notFound(req, res)
})

/**
 * Dashboard
 */
router.get('/', (req, res) => {
  res.redirect('./dashboard')
})

router.get('/dashboard', (req, res) => {
  adminPageController.dashboard(req, res)
})

/**
 * Categories
 */
router.get('/categories/', (req, res) => {
  res.redirect('./list')
})

router.get('/categories/list', (req, res) => {
  adminPageController.categories.list(req, res)
})

router.get('/categories/new', (req, res) => {
  adminPageController.categories.new(req, res)
})

router.get('/categories/edit/:id', (req, res) => {
  adminPageController.categories.edit(req, res)
})

/**
 * Team
 */
router.get('/teams/', (req, res) => {
  res.redirect('./list')
})

router.get('/teams/list', (req, res) => {
  adminPageController.teams.list(req, res)
})

router.get('/teams/new', (req, res) => {
  adminPageController.teams.new(req, res)
})

router.get('/teams/edit/:id', (req, res) => {
  adminPageController.teams.edit(req, res)
})

/**
 * Results
 */
router.get('/results/', (req, res) => {
  res.redirect('./list')
})

router.get('/results/list', (req, res) => {
  adminPageController.results.list(req, res)
})

router.get('/results/new', (req, res) => {
  adminPageController.results.new(req, res)
})

router.get('/results/edit/:id', (req, res) => {
  adminPageController.results.edit(req, res)
})


/**
 * Users
 */
router.get('/users/', (req, res) => {
  res.redirect('./list')
})

router.get('/users/list', (req, res) => {
  adminPageController.users.list(req, res)
})

router.get('/users/new', (req, res) => {
  adminPageController.users.new(req, res)
})

router.get('/users/edit/:id', (req, res) => {
  adminPageController.users.edit(req, res)
})

/**
 * Years
 */
router.get('/years/', (req, res) => {
  res.redirect('./list')
})

router.get('/years/list', (req, res) => {
  adminPageController.years.list(req, res)
})

router.get('/years/new', (req, res) => {
  adminPageController.years.new(req, res)
})

router.get('/years/edit/:id', (req, res) => {
  adminPageController.years.edit(req, res)
})

/**
 * Profile
 */
router.get('/profile', (req, res) => {
  adminPageController.profile(req, res)
})

/**
 * Error pages for test
 */
router.get('/403', (req, res) => {
  adminPageController.error.accessDenied(req, res)
})

router.get('/404', (req, res) => {
  adminPageController.error.notFound(req, res)
})

router.get('/500', (req, res) => {
  adminPageController.error.internalError(req, res)
})

router.get('*', (req, res) => {
  adminPageController.error.notFound(req, res)
})

module.exports = router
