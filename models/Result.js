/**
 * Result database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */

// library for easy database manipulations
const mongoose = require('../libs/db')

// the schema itself
var resultSchema = new mongoose.Schema({
  attempts: [Number],
  priority: {
    type: Number,
    default: 0
  },
  type: {
    type: String,
    enum: ['qualification', 'spider']
  },
  style: {
    type: String,
    enum: ['top', 'bottom'],
    default: 'top'
  },
  team: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Team',
    required: true
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category',
    required: true
  },
  year: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Year',
    required: true
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
})

// export
module.exports = mongoose.model('Result', resultSchema, 'result')
